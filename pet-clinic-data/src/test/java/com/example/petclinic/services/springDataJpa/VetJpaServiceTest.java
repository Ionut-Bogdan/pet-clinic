package com.example.petclinic.services.springDataJpa;

import com.example.petclinic.model.Vet;
import com.example.petclinic.repositories.VetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VetJpaServiceTest {

    @Mock
    VetRepository vetRepository;

    @InjectMocks
    VetJpaService service;

    Vet returnedVet;
    final Long vetId = 1L;

    @BeforeEach
    void setUp() {
        returnedVet = Vet.builder().id(vetId).build();
    }

    @Test
    void findAll() {
        Set<Vet> vets = new HashSet<>();
        vets.add(Vet.builder().build());
        vets.add(Vet.builder().build());

        when(vetRepository.findAll()).thenReturn(vets);

        Set<Vet> returnedVets = service.findAll();
        assertNotNull(returnedVets);
        assertEquals(2, returnedVets.size());
    }

    @Test
    void findById() {
        when(vetRepository.findById(anyLong())).thenReturn(Optional.of(returnedVet));

        Vet vet = service.findById(vetId);
        assertNotNull(vet);
    }

    @Test
    void findByIdNotFound() {
        when(vetRepository.findById(anyLong())).thenReturn(Optional.empty());

        Vet vet = service.findById(vetId);
        assertNull(vet);
    }

    @Test
    void save() {
        Vet vetToSave = Vet.builder().id(2L).build();

        when(vetRepository.save(any())).thenReturn(vetToSave);

        Vet vet = service.save(vetToSave);
        assertNotNull(vet);
        assertEquals(2, vet.getId());
    }

    @Test
    void delete() {
        service.delete(returnedVet);

        verify(vetRepository).delete(any());
        assertEquals(0, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(vetId);

        verify(vetRepository).deleteById(anyLong());
        assertEquals(0, service.findAll().size());
    }
}