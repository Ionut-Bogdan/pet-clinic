package com.example.petclinic.services.springDataJpa;

import com.example.petclinic.model.Pet;
import com.example.petclinic.repositories.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PetJpaServiceTest {

    @Mock
    PetRepository petRepository;

    @InjectMocks
    PetJpaService service;

    Pet returnedPet;
    final Long petId = 1L;

    @BeforeEach
    void setUp() {
        returnedPet = Pet.builder().id(petId).build();
    }

    @Test
    void findAll() {
        Set<Pet> petsToSave = new HashSet<>();
        petsToSave.add(Pet.builder().id(1L).build());
        petsToSave.add(Pet.builder().id(2L).build());

        when(petRepository.findAll()).thenReturn(petsToSave);

        Set<Pet> pets = service.findAll();
        assertNotNull(pets);
        assertEquals(2, pets.size());
    }

    @Test
    void findById() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.of(returnedPet));

        Pet pet = service.findById(petId);
        assertNotNull(pet);
    }

    @Test
    void findByIdNotFound() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());

        Pet pet = service.findById(petId);
        assertNull(pet);
    }

    @Test
    void save() {
        Pet petToSave = Pet.builder().id(2L).build();

        when(petRepository.save(any())).thenReturn(petToSave);

        Pet savedPet = service.save(petToSave);

        assertEquals(2L, savedPet.getId());
        assertNotNull(savedPet);
    }

    @Test
    void delete() {
        service.delete(returnedPet);

        verify(petRepository).delete(any());
        assertEquals(0, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(petId);

        verify(petRepository).deleteById(anyLong());
        assertEquals(0, service.findAll().size());
    }
}