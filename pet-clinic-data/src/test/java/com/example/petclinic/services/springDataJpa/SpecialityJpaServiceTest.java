package com.example.petclinic.services.springDataJpa;

import com.example.petclinic.model.Speciality;
import com.example.petclinic.repositories.SpecialityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SpecialityJpaServiceTest {

    @Mock
    SpecialityRepository specialityRepository;

    @InjectMocks
    SpecialityJpaService service;

    Speciality returnedSpeciality;
    final Long specialityId = 1L;

    @BeforeEach
    void setUp() {
        returnedSpeciality = Speciality.builder().id(specialityId).build();
    }

    @Test
    void findAll() {
        Set<Speciality> specialities = new HashSet<>();
        specialities.add(Speciality.builder().build());
        specialities.add(Speciality.builder().build());

        when(specialityRepository.findAll()).thenReturn(specialities);

        Set<Speciality> returnedSpecialities = service.findAll();

        assertEquals(2, returnedSpecialities.size());
        assertNotNull(returnedSpecialities);
    }

    @Test
    void findById() {
        when(specialityRepository.findById(anyLong())).thenReturn(Optional.of(returnedSpeciality));

        Speciality speciality = service.findById(specialityId);
        assertNotNull(speciality);
    }

    @Test
    void findByIdNotFound() {
        when(specialityRepository.findById(anyLong())).thenReturn(Optional.empty());

        Speciality speciality = service.findById(specialityId);
        assertNull(speciality);
    }

    @Test
    void save() {
        Speciality specialityToSave = Speciality.builder().id(2L).build();

        when(specialityRepository.save(any())).thenReturn(specialityToSave);

        Speciality speciality = service.save(specialityToSave);
        assertNotNull(speciality);
        assertEquals(2, speciality.getId());
    }

    @Test
    void delete() {
        service.delete(returnedSpeciality);

        verify(specialityRepository).delete(any());
        assertEquals(0, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(specialityId);

        verify(specialityRepository).deleteById(anyLong());
        assertEquals(0, service.findAll().size());
    }
}