package com.example.petclinic.services.map;

import com.example.petclinic.model.Vet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class VetMapServiceTest {

    VetMapService vetMapService;
    SpecialityMapService specialityMapService;
    final Long vetId = 1L;

    @BeforeEach
    void setUp() {
        specialityMapService = new SpecialityMapService();
        vetMapService = new VetMapService(specialityMapService);
        vetMapService.save(Vet.builder().id(vetId).specialities(specialityMapService.findAll()).build());
    }

    @Test
    void findAll() {
        Set<Vet> vets = vetMapService.findAll();
        assertEquals(1, vets.size());
    }

    @Test
    void findById() {
        Vet vet = vetMapService.findById(vetId);
        assertEquals(vetId, vet.getId());
    }

    @Test
    void saveExistingId() {
        Long id = 2L;

        Vet vet = Vet.builder().id(id).specialities(specialityMapService.findAll()).build();
        Vet savedVet = vetMapService.save(vet);

        assertEquals(id, savedVet.getId());
    }

    @Test
    void saveNoId() {
        Vet vet = vetMapService.save(Vet.builder().specialities(specialityMapService.findAll()).build());

        assertNotNull(vet);
        assertNotNull(vet.getId());
    }

    @Test
    void delete() {
        vetMapService.delete(vetMapService.findById(vetId));
        assertEquals(0, vetMapService.findAll().size());
    }

    @Test
    void deleteById() {
        vetMapService.deleteById(vetId);
        assertEquals(0, vetMapService.findAll().size());
    }
}