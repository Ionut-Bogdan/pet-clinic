package com.example.petclinic.services.map;

import com.example.petclinic.model.Owner;
import com.example.petclinic.model.Pet;
import com.example.petclinic.model.Visit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class VisitMapServiceTest {

    VisitMapService visitMapService;
    final Long visitId = 1L;
    final Long ownerId = 1L;
    final Long petId = 1L;

    @BeforeEach
    void setUp() {
        visitMapService = new VisitMapService();

        Owner owner = Owner.builder().id(ownerId).build();
        Pet pet = Pet.builder().id(petId).owner(owner).build();
        visitMapService.save(Visit.builder().id(visitId).pet(pet).build());
    }

    @Test
    void findAll() {
        Set<Visit> visits = visitMapService.findAll();

        assertEquals(1, visits.size());
    }

    @Test
    void findById() {
        Visit visit = visitMapService.findById(visitId);

        assertEquals(visitId, visit.getId());
    }

    @Test
    void saveExistingId() {
        Long id = 2L;

        Owner owner = Owner.builder().id(ownerId).build();
        Pet pet = Pet.builder().id(petId).owner(owner).build();
        Visit visit = Visit.builder().id(id).pet(pet).build();

        Visit savedVisit = visitMapService.save(visit);
        assertEquals(id, savedVisit.getId());
    }

    @Test
    void saveNoId() {
        Owner owner = Owner.builder().id(ownerId).build();
        Pet pet = Pet.builder().id(petId).owner(owner).build();
        Visit visit = Visit.builder().pet(pet).build();

        Visit savedVisit = visitMapService.save(visit);
        assertNotNull(savedVisit);
        assertNotNull(savedVisit.getId());
    }

    @Test
    void deleteById() {
        visitMapService.deleteById(visitId);
        assertEquals(0, visitMapService.findAll().size());
    }

    @Test
    void delete() {
        visitMapService.delete(visitMapService.findById(visitId));
        assertEquals(0, visitMapService.findAll().size());
    }
}