package com.example.petclinic.services.map;

import com.example.petclinic.model.PetType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PetTypeMapServiceTest {

    PetTypeMapService petTypeMapService;
    final Long petTypeId = 1L;

    @BeforeEach
    void setUp() {
        petTypeMapService = new PetTypeMapService();
        petTypeMapService.save(PetType.builder().id(petTypeId).build());
    }

    @Test
    void findAll() {
        Set<PetType> petTypes = petTypeMapService.findAll();
        assertEquals(1, petTypes.size());
    }

    @Test
    void findById() {
        PetType petType = petTypeMapService.findById(petTypeId);
        assertEquals(petTypeId, petType.getId());
    }

    @Test
    void saveExistingId() {
        Long id = 2L;

        PetType petType = PetType.builder().id(id).build();
        PetType savedPetType = petTypeMapService.save(petType);

        assertEquals(id, savedPetType.getId());
    }

    @Test
    void saveNoId() {
        PetType savedPetType = petTypeMapService.save(PetType.builder().build());

        assertNotNull(savedPetType);
        assertNotNull(savedPetType.getId());
    }

    @Test
    void deleteById() {
        petTypeMapService.deleteById(petTypeId);
        assertEquals(0, petTypeMapService.findAll().size());
    }

    @Test
    void delete() {
        petTypeMapService.delete(petTypeMapService.findById(petTypeId));
        assertEquals(0, petTypeMapService.findAll().size());
    }
}