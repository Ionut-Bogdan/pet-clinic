package com.example.petclinic.services.springDataJpa;

import com.example.petclinic.model.PetType;
import com.example.petclinic.repositories.PetTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PetTypeJpaServiceTest {

    @Mock
    PetTypeRepository petTypeRepository;

    @InjectMocks
    PetTypeJpaService service;

    PetType petTypeReturned;
    final Long petTypeId = 1L;

    @BeforeEach
    void setUp() {
        petTypeReturned = PetType.builder().id(petTypeId).build();
    }

    @Test
    void findAll() {
        Set<PetType> petTypeSet = new HashSet<>();
        petTypeSet.add(PetType.builder().build());
        petTypeSet.add(PetType.builder().build());

        when(petTypeRepository.findAll()).thenReturn(petTypeSet);

        Set<PetType> returnedPetTypeSet = service.findAll();
        assertNotNull(returnedPetTypeSet);
        assertEquals(2, returnedPetTypeSet.size());
    }

    @Test
    void findById() {
        when(petTypeRepository.findById(anyLong())).thenReturn(Optional.of(petTypeReturned));

        PetType petType = service.findById(petTypeId);
        assertNotNull(petType);
        assertEquals(petTypeId, petType.getId());
    }

    @Test
    void findByIdNotFound() {
        when(petTypeRepository.findById(anyLong())).thenReturn(Optional.empty());

        PetType petType = service.findById(petTypeId);
        assertNull(petType);
    }

    @Test
    void save() {
        PetType petTypeToSave = PetType.builder().id(2L).build();

        when(petTypeRepository.save(any())).thenReturn(petTypeToSave);

        PetType savedPetType = service.save(petTypeToSave);

        assertEquals(2, savedPetType.getId());
        assertNotNull(savedPetType);
    }

    @Test
    void delete() {
        service.delete(petTypeReturned);

        verify(petTypeRepository).delete(any());
        assertEquals(0, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(petTypeId);

        verify(petTypeRepository).deleteById(anyLong());
        assertEquals(0, service.findAll().size());
    }
}