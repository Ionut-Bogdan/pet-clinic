package com.example.petclinic.services.springDataJpa;

import com.example.petclinic.model.Visit;
import com.example.petclinic.repositories.VisitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VisitJpaServiceTest {

    @Mock
    VisitRepository visitRepository;

    @InjectMocks
    VisitJpaService service;

    Visit returnedVisit;
    final Long visitId = 1L;

    @BeforeEach
    void setUp() {
        returnedVisit = Visit.builder().id(visitId).build();
    }

    @Test
    void findAll() {
        Set<Visit> returnedVisits = new HashSet<>();
        returnedVisits.add(Visit.builder().build());
        returnedVisits.add(Visit.builder().build());

        when(visitRepository.findAll()).thenReturn(returnedVisits);

        Set<Visit> visits = service.findAll();
        assertNotNull(visits);
        assertEquals(2, visits.size());
    }

    @Test
    void findById() {
        when(visitRepository.findById(anyLong())).thenReturn(Optional.of(returnedVisit));

        Visit visit = service.findById(visitId);
        assertNotNull(visit);
    }

    @Test
    void findByIdNotFound() {
        when(visitRepository.findById(anyLong())).thenReturn(Optional.empty());

        Visit visit = service.findById(visitId);
        assertNull(visit);
    }

    @Test
    void save() {
        Visit visitToSave = Visit.builder().id(2L).build();

        when(visitRepository.save(any())).thenReturn(visitToSave);

        Visit visit = service.save(visitToSave);
        assertNotNull(visit);
        assertEquals(2, visit.getId());
    }

    @Test
    void delete() {
        service.delete(returnedVisit);

        verify(visitRepository).delete(any());
        assertEquals(0, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(visitId);

        verify(visitRepository).deleteById(anyLong());
        assertEquals(0, service.findAll().size());
    }
}