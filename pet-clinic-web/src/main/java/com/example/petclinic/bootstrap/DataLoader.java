package com.example.petclinic.bootstrap;

import com.example.petclinic.model.*;
import com.example.petclinic.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialityService specialityService;
    private final VisitService visitService;

    @Autowired
    public DataLoader(OwnerService ownerService,
                      VetService vetService,
                      PetTypeService petTypeService,
                      SpecialityService specialityService,
                      VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialityService = specialityService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {

        int count = petTypeService.findAll().size();

        if (count == 0) {
            loadData();
        }
    }

    private void loadData() {

        PetType dog = new PetType();
        dog.setName("Dog");
        PetType savedDogPetType = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("Cat");
        PetType savedCatPetType = petTypeService.save(cat);

        Speciality radiology = new Speciality();
        radiology.setDescription("Radiology");
        Speciality savedRadiology = specialityService.save(radiology);

        Speciality surgery = new Speciality();
        surgery.setDescription("Surgery");
        Speciality savedSurgery = specialityService.save(surgery);

        Speciality dentistry = new Speciality();
        dentistry.setDescription("Dentistry");
        Speciality savedDentistry = specialityService.save(dentistry);

        Owner owner1 = new Owner();
        owner1.setFirstName("Bogdan");
        owner1.setLastName("Cristea");
        owner1.setAddress("Str. Cantemir");
        owner1.setCity("Iasi");
        owner1.setTelephone("123456789");

        Pet bogdansPet = new Pet();
        bogdansPet.setPetType(savedDogPetType);
        bogdansPet.setOwner(owner1);
        bogdansPet.setBirthDate(LocalDate.now());
        bogdansPet.setName("Rover");
        owner1.getPets().add(bogdansPet);
        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Neculai");
        owner2.setLastName("Matei");
        owner2.setAddress("Str. Bucium");
        owner2.setCity("Iasi");
        owner2.setTelephone("234567891");

        Pet neculaisCat = new Pet();
        neculaisCat.setName("Pufi");
        neculaisCat.setOwner(owner2);
        neculaisCat.setBirthDate(LocalDate.now());
        neculaisCat.setPetType(savedCatPetType);
        owner2.getPets().add(neculaisCat);
        ownerService.save(owner2);

        Visit catVisit = new Visit();
        catVisit.setPet(neculaisCat);
        catVisit.setDate(LocalDate.now());
        catVisit.setDescription("Regular visit");

        visitService.save(catVisit);

        System.out.println("Loaded Owners...");

        Vet vet1 = new Vet();
        vet1.setFirstName("Maria");
        vet1.setLastName("Barcan");
        vet1.getSpecialities().add(savedRadiology);
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Catalin");
        vet2.setLastName("Cantemir");
        vet2.getSpecialities().add(savedSurgery);
        vetService.save(vet2);

        System.out.println("Loading Vets...");
    }
}
